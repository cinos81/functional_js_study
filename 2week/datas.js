export const datas = [
    {
        is_popular: true,
        name: "반팔티",
        price: 10000,
        sizes: [
            { name: "L", quantity: 2, price: 0 },
            { name: "XL", quantity: 3, price: 0 },
            { name: "2XL", quantity: 2, price: 2000 }
        ]
    },
    {
        is_popular: false,
        name: "긴팔티",
        price: 31000,
        sizes: [
            { name: "L", quantity: 2, price: 0 },
            { name: "XL", quantity: 3, price: 1000 },
            { name: "2XL", quantity: 2, price: 2000 }
        ]
    },
    {
        is_popular: true,
        name: "후드티",
        price: 21000,
        sizes: [
            { name: "L", quantity: 0, price: 0 },
            { name: "XL", quantity: 5, price: 1000 },
            { name: "2XL", quantity: 10, price: 2000 }
        ]
    },
    {
        is_popular: false,
        name: "맨투맨",
        price: 16000,
        sizes: [
            { name: "L", quantity: 4, price: 0 }
        ]
    },
    {
        is_popular: true,
        name: "기모후드티",
        price: 26000,
        sizes: [
            { name: "L", quantity: 3, price: -1000 },
            { name: "2XL", quantity: 4, price: 2000 }
        ]
    },
    {
        is_popular: false,
        name: "기모맨투맨",
        price: 21000,
        sizes: [
            { name: "M", quantity: 0, price: 0 },
            { name: "L", quantity: 0, price: 0 },
            { name: "XL", quantity: 0, price: 2000 }
        ]
    }
];